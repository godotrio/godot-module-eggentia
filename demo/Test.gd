extends Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	var track_filename = "jenkees.mp3"
	
	print("Testing Essentia#get_duration…")
	var e = Eggentia.new()
	var duration = e.get_duration(track_filename)
	print(duration)
	
	print("Testing Essentia#get_beats…")
	var beats = e.get_beats(track_filename)
	print(beats)

extends Control

onready var BeatReader = preload("res://BeatReader.gd")
onready var play_button = $"/root/Tool/Panel/PlayButton"
onready var audio_stream_player = $"/root/Tool/AudioStreamPlayer"
onready var logo = $"/root/Tool/Panel/Logo"
var beats
var beat_reader
var start_time
var stream_playing = false
var alpha = 1.0
var scale = 1.0
var base_position
#var filepath


func load_audio_file(filepath):
	if filepath.ends_with('ogg'):
		var file = File.new()
		file.open(filepath, File.READ)
		var oggstream = AudioStreamOGGVorbis.new()
		oggstream.data = file.get_buffer(file.get_len())
		audio_stream_player.stream = oggstream
		play_button.connect("pressed", self, "play_stream")
	else:
		push_warning("Only OGG Vorbis files can be played at the moment.")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	logo.modulate = Color(1.0 , 1.0, 1.0, alpha)
	
	if stream_playing:
		alpha *= 0.66
		scale *= 0.97
		scale = min(0.5, scale)
#		alpha = min(alpha, 1.0)
		var current_app_time = OS.get_ticks_usec()
		#print(beats)
		var current_time = (current_app_time - start_time) / 1000000.0
#		print(logo.rect_size)
		if beat_reader.get_beat_intensity(current_time, beats) > 0.1:
			alpha = 1.0
			scale = 0.67
#			logo.visible = false
		else:
#			logo.visible = true
			pass
		
		logo.rect_scale = Vector2(scale, scale)
		logo.rect_position = base_position - (logo.rect_size * scale) / 2.0

func play_stream():
	beat_reader = BeatReader.new()
	start_time = OS.get_ticks_usec()
	audio_stream_player.play(0.0)
	stream_playing = true

# Called when the node enters the scene tree for the first time.
func _ready():
	base_position = logo.rect_position + logo.rect_size/3.2
	pass # Replace with function body.


#!/usr/bin/env bash

# Gotta have that godot build with the modules.
# Still trying to make a Dockerfile generating it.
GODOT=~/palace/bin/godot/3.1.1-modot0/godot.x11.tools.64

VERSION=$1
if [ -z $VERSION ] ; then
  echo "Usage: ./build.sh VERSION"
  exit 1
fi

echo "Building for $VERSION…"

$GODOT --export Linux32 edgentia.linux32.$VERSION.bin
$GODOT --export Linux64 edgentia.linux64.$VERSION.bin
$GODOT --export Windows32 edgentia.windows32.$VERSION.exe
$GODOT --export Windows32 edgentia.windows64.$VERSION.exe

extends Button


onready var analyze_button = $"."
onready var tool_node = $"/root/Tool"
onready var file_dialog = $"../FileDialog"
onready var logger = $"/root/Tool/Panel/LoggerScroll/LoggerPanel/Logger"


func _pressed():
	file_dialog.connect("file_selected", self, "request_analysis")
	file_dialog.popup_centered()

var analyzer

func request_analysis(filepath):
	tool_node.load_audio_file(filepath)
	tool_node.play_button.disabled = true
	analyze_button.disabled = true
	if analyzer and analyzer.is_active():
		analyzer.wait_to_finish()
#		print("Wow. You managed to do something quite unexpected. Congratulations!")
#		return

	# Analysis is going to take some time.
	# Using Threads don't freeze the UI.
	analyzer = Thread.new()
	analyzer.start(self, 'analyze', filepath)


func analyze(filepath):
	
	logger.text = """
COMPUTING!
PLEASE READ SOME POETRY WHILE WAITING.

Le temps perdu

Devant la porte de l'usine
le travailleur soudain s'arrête
le beau temps l'a tiré par la veste
et comme il se retourne
et regarde le soleil
tout rouge tout rond
souriant dans son ciel de plomb
il cligne de l'oeil
familièrement
Dis donc camarade Soleil
tu ne trouves pas
que c'est plutôt con
de donner une journée pareille
à un patron ?

— Jacques Prévert
"""
	
	print("Starting analysis of '%s'…" % filepath)
	
	var egg = Eggentia.new()
	
	var duration = egg.get_duration(filepath)
	var beats = egg.get_beats(filepath)
	tool_node.beats = beats
	var data = {
		'duration': duration,
		'beats': beats,
	}
	
	logger.text = var2str(data)
	print(data)
	
	analyze_button.disabled = false
	tool_node.play_button.disabled = false
	

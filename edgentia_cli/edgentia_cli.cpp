/*
 * Extract useful information from an audio file.
 * Our needs are those of game developers.
 * 
 * - duration (s)
 * - positions of the beats (s)
 * 
 * 
 * Compile it along with the examples of essentia, should work.
 * - copy it into <essentia>/src/examples/
 * - add it to whitelist in src/wscript
 * - ./waf configure --build-static --with-static-examples
 * - ./waf configure --build-static --with-examples
 * # TODO check which
 * - ./waf -v
 * 
 * ---
 * 
 * Copyright (C) 2019 The Internet, months before the jubilee of the Information Era.
 * 
 * This file is made from parts of Essentia glued together with the elegance of a mammoth on a sophisticated trampoline.  In zero gravity.  With lasers.
 *
 * ---
 * 
 * Copyright (C) 2006-2016  Music Technology Group - Universitat Pompeu Fabra
 * 
 * Essentia is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation (FSF), either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * version 3 along with this program.  If not, see http://www.gnu.org/licenses/
 */

#include <iostream>
#include <fstream>
#include <essentia/algorithmfactory.h>

using namespace std;
using namespace essentia;
using namespace standard;

int main(int argc, char* argv[]) {

    if (2 != argc) {
        cout << "°º¤ø,¸¸,ø¤º°`°º¤ø,¸,ø¤°º¤ø,¸¸,ø¤º°`°º¤ø,¸" << endl << endl;
        cout << "Usage: " << argv[0] << " audio_input_file" << endl << endl;
        cout << "Goal: Prints some properties like duration and detected beats" << endl;
        cout << "      of an input audio file (eg: mp3), in YAML to stdout." << endl << endl;
        cout << "Source: https://framagit.org/godotrio/godot-module-eggentia" << endl;
        cout << "Author: My Sweet Little Angel of Lime <contact@ljbac.com>" << endl;
        cout << "Version: 0.1.0" << endl; // use DEFINED constant for version?
        
        exit(1);
    }
    
    try {

        string audioFilename = argv[1];

        essentia::init();

        Real sampleRate = 44100.0;

        AlgorithmFactory& factory = AlgorithmFactory::instance();

        // I. Fetch the audio as a signal from the file

        Algorithm* audioLoader = factory.create("MonoLoader",
            "filename", audioFilename,
            "sampleRate", sampleRate
        );
        vector<Real> audio;

        audioLoader->output("audio").set(audio);
        audioLoader->compute();

        // II. Get the duration of the track in seconds

        essentia::standard::Algorithm* durationAlgo = factory.create("Duration");

        essentia::Real duration;

        durationAlgo->input("signal").set(audio);
        durationAlgo->output("duration").set(duration);
        durationAlgo->compute();

        // III. Run a beat tracker

        Algorithm* beatTracker = factory.create("BeatTrackerMultiFeature");

        vector<Real> beats;
        Real confidence;

        beatTracker->input("signal").set(audio);
        beatTracker->output("ticks").set(beats);
        beatTracker->output("confidence").set(confidence);
        beatTracker->compute();

        // IV. Tonal Extractor

        Algorithm* tonalExtractor = factory.create("TonalExtractor");

        Real chords_changes_rate;
        vector<Real> chords_histogram;
        std::string chords_key;
        Real chords_number_rate;
        vector<std::string> chords_progression;
        std::string chords_scale;
        vector<Real> chords_strength;
        vector<vector<Real>> hpcp;
        vector<vector<Real>> hpcp_highres;
        std::string key_key;
        std::string key_scale;
        Real key_strength;
        
        tonalExtractor->input("signal").set(audio);
        tonalExtractor->output("chords_changes_rate").set(chords_changes_rate);
        tonalExtractor->output("chords_histogram").set(chords_histogram);
        tonalExtractor->output("chords_key").set(chords_key);
        tonalExtractor->output("chords_number_rate").set(chords_number_rate);
        tonalExtractor->output("chords_progression").set(chords_progression);
        tonalExtractor->output("chords_scale").set(chords_scale);
        tonalExtractor->output("chords_strength").set(chords_strength);
        tonalExtractor->output("hpcp").set(hpcp);
        tonalExtractor->output("hpcp_highres").set(hpcp_highres);
        tonalExtractor->output("key_key").set(key_key);
        tonalExtractor->output("key_scale").set(key_scale);
        tonalExtractor->output("key_strength").set(key_strength);
        tonalExtractor->compute();

        // IV. Do more things
        // …

        // Eventually, print the goodies to stdout
        
        cout << "duration: " << duration << endl;
        cout << "beats: " << beats << endl;
        cout << "chords:" << endl;
        cout << "  changes_rate: " << chords_changes_rate << endl;
        cout << "  strength: " << chords_strength << endl;
        cout << "  progression: " << chords_progression << endl;
        cout << "  histogram: " << chords_histogram << endl;
        cout << "  hpcp: " << hpcp << endl;
        
        // And close up shop

        delete audioLoader;
        delete durationAlgo;
        delete beatTracker;
        delete tonalExtractor;

        essentia::shutdown();
    
    }
    catch (essentia::EssentiaException& err) {
        //cout << "error: " << err.what() << endl;
        // Writing to cerr instead of cout
        cerr << "error: " << err.what() << endl;
        
        exit(2);
    }
    
    return 0;
}


## Use

- [install essentia](https://essentia.upf.edu/documentation/installing.html) (about four commands and a 15min compilation)
- clone godot
- clone this
- go into this module's repository that you just cloned
- ignore the `demo` directory for now
- link or copy `eggentia` into your godot's clone's `modules/eggentia`
- compile godot _and export templates_
- run the demo (two scenes -- test and tool)


## Features

```gd
	// Example usage in GdScript
	var egg = Eggentia.new()
	var duration = egg.get_duration("jenkees.mp3")
	print(duration)  // 215.45475 (s)
```

- `get_duration(String track_filepath)` (Real)
- `get_beats(String track_filepath)` (Array of Real)
- …

[Essentia](https://essentia.upf.edu) has *many* features left to bridge|facade into Godot.

> Everything is in seconds.  Working with floating point numbers is tricky ; expect oddities. (2.675)


## Roadmap

None.


## Disclaimer

This is an experiment, and it's possibly full of bugs, security issues and memleaks.

Reviews and tips are appreciated :)

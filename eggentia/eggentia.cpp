#include "eggentia.h"


std::string Eggentia::ws2s(const std::wstring &wstr)
{
    using convert_type = std::codecvt_utf8<wchar_t>;
    std::wstring_convert<convert_type, wchar_t> converter;

    return converter.to_bytes(wstr);
}


std::string Eggentia::gs2s(const String &gstr)
{
    return Eggentia::ws2s(gstr.ptr());
}


String Eggentia::s2gs(const std::string &str)
{
    return String::utf8(str.c_str());
}

Array Eggentia::vf2a(std::vector<float> &vf)
{
    Array array = Array();

    for (auto & element : vf) {
        array.push_back(element);
    }

    return array;
}


/*
String Ginja::render(const String &gs_template, const Dictionary &variables)
{
    const std::string s_template = Ginja::gs2s(gs_template);

    // To pivot our Dictionary of Variants to a nlohmann::json object,
    // we're using serialization and then deserialization. It's cheap.
    // There may be drawbacks, though. Bigger memory footprint, etc.
    // This has not been extensively tested. Contributions are welcome :3

    // First we serialize our Dictionary using Godot's JSON.
    const String gs_variables = JSON::print(variables);

    // print_line(j_variables);

    // Then we deserialize using nlohmann::json (injason)
    injason data = injason::parse(Ginja::gs2s(gs_variables));

    // Now we have everything we need to ask Inja to render
    std::string ss_return = inja::render(s_template, data);

    //fprintf(stdout, "[ginja] Rendered:\n%s\n", ss_return.c_str());

    return String::utf8(ss_return.c_str());
}
*/

float Eggentia::get_duration(const String &track_filepath) {
    //return 42.17;
    
    try {
//       fprintf(stdout, "[eggentia] Computing duration of\n");
        
        essentia::init();
        
        essentia::Real sampleRate = 44100.0;

        essentia::standard::AlgorithmFactory& factory = essentia::standard::AlgorithmFactory::instance();
        
        essentia::standard::Algorithm* audioLoader = factory.create("MonoLoader",
            "filename", Eggentia::gs2s(track_filepath),
            "sampleRate", sampleRate
        );
        
        std::vector<essentia::Real> audio;
        audioLoader->output("audio").set(audio);
        audioLoader->compute();
        

        
        essentia::standard::Algorithm* durationAlgo = factory.create("Duration");
        
        //essentia::standard::Algorithm* beatTracker = factory.create("BeatTrackerMultiFeature");

        
        essentia::Real duration;
        
        durationAlgo->input("signal").set(audio);
        durationAlgo->output("duration").set(duration);
        durationAlgo->compute();
        
        return duration;
    }
    catch (essentia::EssentiaException& err) {
        char error_buffer[1024];
        // Neither stderr nor stdout print to Godot's internal console. How do we do this?
        //fprintf(stderr, "[eggentia] Error with Essentia: %s\n", err.what());
        sprintf(error_buffer, "[eggentia] Error with Essentia: %s\n", err.what());
        //print_line(error_buffer);
        print_error(error_buffer);
        
        return 0;
    }
}



Array Eggentia::get_beats(const String &track_filepath) {
    
    try {
//       fprintf(stdout, "[eggentia] Computing duration of\n");
        
        essentia::init();
        
        essentia::Real sampleRate = 44100.0;

        essentia::standard::AlgorithmFactory& factory = essentia::standard::AlgorithmFactory::instance();
        
        essentia::standard::Algorithm* audioLoader = factory.create("MonoLoader",
            "filename", Eggentia::gs2s(track_filepath),
            "sampleRate", sampleRate
        );
        
        std::vector<essentia::Real> audio;
        audioLoader->output("audio").set(audio);
        audioLoader->compute();
        
        
        essentia::standard::Algorithm* beatTracker = factory.create("BeatTrackerMultiFeature");

        std::vector<essentia::Real> beats;
        essentia::Real confidence;
        beatTracker->input("signal").set(audio);
        beatTracker->output("ticks").set(beats);
        beatTracker->output("confidence").set(confidence);
        beatTracker->compute();
        
        return Eggentia::vf2a(beats);
    }
    catch (essentia::EssentiaException& err) {
        char error_buffer[1024];
        // Neither stderr nor stdout print to Godot's internal console. How do we do this?
        //fprintf(stderr, "[eggentia] Error with Essentia: %s\n", err.what());
        sprintf(error_buffer, "[eggentia] Error with Essentia: %s\n", err.what());
        //print_line(error_buffer);
        print_error(error_buffer);
        
        // free buffer fixme
        
        Array nobeats;
        return nobeats;
    }
}


void Eggentia::_bind_methods()
{
    ClassDB::bind_method(D_METHOD("get_duration", "track_filepath"), &Eggentia::get_duration);
    ClassDB::bind_method(D_METHOD("get_beats", "track_filepath"), &Eggentia::get_beats);
}


Eggentia::Eggentia() {}



def can_build(env, platform):
    # /!. Not all platforms and env have been tested yet.
    #     - Known to work on Debian/Linux
    return True


def configure(env):
    pass


def get_doc_classes():
    return [
        "Eggentia",
    ]


def get_doc_path():
    return "doc_classes"

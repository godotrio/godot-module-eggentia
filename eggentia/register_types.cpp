
#include "core/class_db.h"
#include "register_types.h"
#include "eggentia.h"

void register_eggentia_types()
{
    ClassDB::register_class<Eggentia>();
}

void unregister_eggentia_types()
{
    // nothing is cool
}

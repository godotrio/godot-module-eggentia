#ifndef ESSENTIA_H
#define ESSENTIA_H

#include <string>

#include <locale>
#include <codecvt>
#include <vector>
#include <cstring>
#include <cwchar>

#include <iostream>
#include <fstream>
#include <essentia/algorithmfactory.h>
#include <essentia/pool.h>
#include <essentia/parameter.h>

#include "core/reference.h"
//#include "core/io/json.h"

//#include "nlohmann/json.hpp"
//#include "inja/inja.hpp"
//#include "inja/string_view.hpp"


// Just for convenience
//using injason = nlohmann::json;


class Eggentia : public Reference {
    GDCLASS(Eggentia, Reference);

    //int count;

protected:
    std::string gs2s(const String &gstr);
    std::string ws2s(const std::wstring &wstr);
    String      s2gs(const std::string &str);
    Array       vf2a(std::vector<float> &vf);

    static void _bind_methods();

public:
    //String render(const String &gs_template, const Dictionary &variables);
    
    float get_duration(const String &track_filepath);
    Array get_beats(const String &track_filepath);

    Eggentia();
};

#endif
